drop database audit;

Create database audit;
Use audit;

CREATE USER 'audituser'@'%' IDENTIFIED BY '@hadoop@@';
CREATE USER 'audituser'@'localhost' IDENTIFIED BY '@hadoop@@';
GRANT ALL ON audit.* TO 'audituser'@'%';
CREATE TABLE WfJobaudit1 (
host VARCHAR(130) NOT NULL,
CordinatorId VARCHAR(200) NOT NULL,
Coordinatorname   VARCHAR(200) NOT NULL,
WorkfloID VARCHAR(200) NOT NULL,
Wfname VARCHAR(200) NOT NULL,
ActionName  VARCHAR(800) NOT NULL,
startTime  VARCHAR(200),
endTime VARCHAR(200),
ExtStatus VARCHAR(20),
Jobtype VARCHAR(200),
childJobId VARCHAR(200),
JobName  VARCHAR(200),
username  VARCHAR(200),
Job_Status  VARCHAR(20),
RECORDS_IN  int DEFAULT 0,
RECORDS_OUT  int DEFAULT 0,
MAP_INPUT_RECORDS  int DEFAULT 0,
MAP_OUTPUT_RECORDS  int DEFAULT 0,
REDUCE_INPUT_RECORDS  int DEFAULT 0,
REDUCE_OUTPUT_RECORDS int DEFAULT 0
);
ALTER TABLE WfJobaudit1 DROP PRIMARY KEY;
ALTER TABLE WfJobaudit1   ADD PRIMARY KEY  pk_Unique  (childJobId,CordinatorId(7),WorkfloID(15),Jobname(10),ExtStatus(2));


select * from  WfJobaudit1;