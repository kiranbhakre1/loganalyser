package com.Loganalyser.audit;

import org.apache.commons.collections.IteratorUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Kiran on 6/29/15.
 */
public class JobCounter {
    public static ObjectMapper mapper = new ObjectMapper();
    public static JsonNode jn = null;

    public static String getCounterDetails(String jobId) {

        String Url = "http://" + LogAnalyse.historyServerHost + ":19888/ws/v1/history/mapreduce/jobs/" + jobId + "/counters";
        String jobCounters = "";
        try {


            JsonNode rootNode = mapper.readTree(RestCall.rest(Url));
            //System.out.println(rootNode);
            jn = mapper.readTree(rootNode.path("jobCounters").path("counterGroup").toString());

            List<JsonNode> Countergroup = IteratorUtils.toList(jn.iterator());

            //JobCounter.GetCounter(Countergroup, "HIVE", "RECORDS_IN");
            String RECORDS_IN,RECORDS_OUT,MAP_INPUT_RECORDS,MAP_OUTPUT_RECORDS,REDUCE_INPUT_RECORDS,REDUCE_OUTPUT_RECORDS;
            if(!JobCounter.GetCounter(Countergroup, "HIVE", "RECORDS_IN").isEmpty())
            {
                RECORDS_IN=  JobCounter.GetCounter(Countergroup, "HIVE", "RECORDS_IN");
            }
            else{
                RECORDS_IN="0";
            }
            if(!JobCounter.GetCounter(Countergroup, "HIVE", "RECORDS_OUT").isEmpty())
            {
                RECORDS_OUT=  JobCounter.GetCounter(Countergroup, "HIVE", "RECORDS_OUT");
            }
            else{
                RECORDS_OUT="0";
            }
            if(!JobCounter.GetCounter(Countergroup, "org.apache.hadoop.mapreduce.TaskCounter", "MAP_INPUT_RECORDS").isEmpty())
            {
                MAP_INPUT_RECORDS=  JobCounter.GetCounter(Countergroup, "org.apache.hadoop.mapreduce.TaskCounter", "MAP_INPUT_RECORDS");
            }
            else{
                MAP_INPUT_RECORDS="0";
            }
            if(!JobCounter.GetCounter(Countergroup, "org.apache.hadoop.mapreduce.TaskCounter", "MAP_OUTPUT_RECORDS").isEmpty())
            {
                MAP_OUTPUT_RECORDS=  JobCounter.GetCounter(Countergroup, "org.apache.hadoop.mapreduce.TaskCounter", "MAP_OUTPUT_RECORDS");
            }
            else{
                MAP_OUTPUT_RECORDS="0";
            }
            if(!JobCounter.GetCounter(Countergroup, "org.apache.hadoop.mapreduce.TaskCounter", "REDUCE_INPUT_RECORDS").isEmpty())
            {
                REDUCE_INPUT_RECORDS=  JobCounter.GetCounter(Countergroup, "org.apache.hadoop.mapreduce.TaskCounter", "REDUCE_INPUT_RECORDS");
            }
            else{
                REDUCE_INPUT_RECORDS="0";
            }   if(!JobCounter.GetCounter(Countergroup, "org.apache.hadoop.mapreduce.TaskCounter", "REDUCE_OUTPUT_RECORDS").isEmpty())
            {
                REDUCE_OUTPUT_RECORDS=  JobCounter.GetCounter(Countergroup, "org.apache.hadoop.mapreduce.TaskCounter", "REDUCE_OUTPUT_RECORDS");
            }
            else{
                REDUCE_OUTPUT_RECORDS="0";
            }


      jobCounters= RECORDS_IN+"|"+RECORDS_OUT+"|"+MAP_INPUT_RECORDS+"|"+MAP_OUTPUT_RECORDS+"|"+REDUCE_INPUT_RECORDS+"|"+REDUCE_OUTPUT_RECORDS;
        } catch (Exception ex) {
            System.out.println("job counter is null"+ex.getMessage());
            jobCounters="0|0|0|0|0|0";
        }
        return jobCounters;


    }

    public static String GetCounter(List<JsonNode> Countergroup, String Group, String Countername) {
        String Counter="";
        for (int i = 0; i < Countergroup.size(); i++) {

            if (Countergroup.get(i).get("counterGroupName").asText().replace("\"", "").equalsIgnoreCase(Group)) {
                //System.out.println(Countergroup.get(i).get("counter").getElements());
                List<JsonNode> Counters = IteratorUtils.toList(Countergroup.get(i).get("counter").getElements());
                for (int j = 0; j < Counters.size(); j++) {
                    if (Counters.get(j).get("name").toString().replace("\"", "").toString().equalsIgnoreCase(Countername)) {
                       Counter=Counters.get(j).get("totalCounterValue").toString();
                    }
                }
            }
        }
        return Counter;

    }

}
