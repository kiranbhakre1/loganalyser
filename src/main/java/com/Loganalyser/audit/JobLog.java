package com.Loganalyser.audit;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * Created by Kiran on 6/29/15.
 */
public class JobLog {
    public static ObjectMapper mapper = new ObjectMapper();
    public static   JsonNode jn=null;
    public static String getjobDetails(String jobId) {

       //String Url= "http://"+LogAnalyse.historyServerHost+":19888/ws/v1/history/mapreduce/jobs/"+jobId+"/counters";
       String Url= "http://"+LogAnalyse.historyServerHost+":19888/ws/v1/history/mapreduce/jobs/"+jobId;

        String jobStatus="";
       // System.out.println(Url);
        try{


        JsonNode rootNode = mapper.readTree(RestCall.rest(Url));

        jn = mapper.readTree(rootNode.path("job").toString());
        //System.out.println(jn);
            String Jd, user,state;
            if(!getjobDescription("name").isEmpty())
            {
                Jd=  getjobDescription("name");
            }
            else{
                Jd="null";
            }
            if(!getjobDescription("user").isEmpty())
            {
                user=  getjobDescription("user");
            }
            else{
                user="null";
            }
            if(!getjobDescription("state").isEmpty())
            {
                state=  getjobDescription("state");
            }
            else{
                state="null";
            }

            jobStatus= "\""+Jd+"\""+ "|"+"\""+user+"\""+"|"+"\""+state+"\"";
        }
        catch (Exception ex){
            //System.out.println("JOb is null");
            jobStatus= "\"null\""+ "|"+"\"null\""+"|"+"\"null\"";
        }
        return jobStatus;
    }


    public static String getjobDescription(String property ){

        return jn.get(property).toString().replace("\"", "").trim();

    }
}
