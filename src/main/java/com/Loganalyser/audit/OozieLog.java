package com.Loganalyser.audit;

import org.apache.commons.collections.IteratorUtils;
import org.codehaus.jackson.JsonNode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kiran on 6/29/15.
 */
public class OozieLog {


    public static void getWokflowLog(String Host, String WorkfloID, String CordinatorId, File file,String Coordinatorname) throws Exception
    {
        // System.out.println();
        JsonNode ooziejn1= null;
        String oozieUrl="http://"+Host+":11000/oozie/v1/job/" +WorkfloID;
        JsonNode oozieNode = LogAnalyse.mapper.readTree(RestCall.rest(oozieUrl));
        JsonNode oozie_jn = LogAnalyse.mapper.readTree(oozieNode.path("actions").toString());

        String appName =oozieNode.get("appName").toString().replace("\"", "").trim();
        List<JsonNode> oozieacions =  IteratorUtils.toList(oozie_jn.getElements());
        //get getwfAction
        for (int i =0; i<oozieacions.size(); i++)
        {
            getwfAction( oozieacions.get(i),Host,WorkfloID,appName,CordinatorId, file,Coordinatorname);
        }

    }


    public static void  getwfAction(JsonNode oozieAcions, String Host, String WorkfloID, String WFname, String CordinatorId,File file, String Coordinatorname ) throws  Exception{

        if(!oozieAcions.get("type").toString().replace("\"", "").trim().equals(":START:") && !oozieAcions.get("type").toString().replace("\"", "").trim().equals(":END:")&& !oozieAcions.get("type").toString().replace("\"", "").trim().equals(":FORK:")&& !oozieAcions.get("type").toString().replace("\"", "").trim().equals(":JOIN:"))
        {
            String Jobname=oozieAcions.get("name").toString().replace("\"", "").trim();
            String startTime=oozieAcions.get("startTime").toString().replace("\"", "").trim();
            String endTime=oozieAcions.get("endTime").toString().replace("\"", "").trim();
            String ExtStatus=oozieAcions.get("externalStatus").toString().replace("\"", "").trim();
            String Jobtype =oozieAcions.get("type").toString().replace("\"", "").trim();
            String [] childJobId =oozieAcions.get("externalChildIDs").toString().replace("\"", "").trim().split(",");

            System.out.println(oozieAcions);


            ArrayList<String> sqlValues= new ArrayList<String>();

            for(int i=0 ;i<childJobId.length; i++)
            {
                if ("SUCCEEDED".equalsIgnoreCase("SUCCEEDED")) {
                    String Log = Host + "|" + CordinatorId + "|" + WorkfloID+"|"+WFname + "|" + Jobname + "|" + startTime + "|" + endTime + "|" + ExtStatus + "|" + Jobtype + "|" + childJobId[i]+"|"+JobLog.getjobDetails(childJobId[i])+"|"+JobCounter.getCounterDetails(childJobId[i]);


                    String SQlValues = "\""+Host + "\""+"|" +"\""+ CordinatorId + "\""+"|" +""+ Coordinatorname +""+
                            "|" +"\""+ WorkfloID +"\""+ "|" +"\""+ WFname +"\""+ "|" +"\""+ Jobname +"\""+ "|" +"\""+ startTime +"\""+
                            "|" +"\""+ endTime +"\""+ "|"+"\"" + ExtStatus +"\""+ "|" +"\""+ Jobtype +"\""+ "|"
                            +"\""+childJobId[i]+"\""+"|"+JobLog.getjobDetails(childJobId[i])+"|"+JobCounter.getCounterDetails(childJobId[i]);


                    System.out.println(SQlValues);
                    if(LogAnalyse.ENABLESQLINSERT)
                    {
                        SqlInsert.SQlinsert(LogAnalyse.SQlname, SQlValues);
                    }
                    WriteToFile.writeData(file,Log);
                    LogAnalyse.insertCounter++;
                }
            }

        }


    }
}
