package com.Loganalyser.audit;//STEP 1. Import required packages
import java.sql.*;

public class SqlInsert {
   // JDBC driver name and database URL
   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
   static final String DB_URL = "jdbc:mysql://"+LogAnalyse.sqlHost+"/"+LogAnalyse.Database;
   //  Database credentials
   static final String USER = LogAnalyse.user;
   static final String PASS =LogAnalyse.password;

   public static String checkempty(String Str){
      if (Str.isEmpty())
      {
         Str="0";
      }
      return Str;
   }
   public static void SQlinsert(String Column, String Values) {
   Connection conn = null;
   Statement stmt = null;
      String Val="";
   try{

      String [] values =Values.split("\\|");
      for (int i=0; i<values.length;i++){
         values[i]=checkempty(values[i]);
        // System.out.println(values[i]);
      }

      for (int i=0; i<values.length;i++){
        if (i==0){
           Val=values[i];
        }
         else {
           Val += "," + values[i];
        }
      }
      //System.out.println(Val);
      //STEP 2: Register JDBC driver
      Class.forName("com.mysql.jdbc.Driver");

      //STEP 3: Open a connection
      conn = DriverManager.getConnection(DB_URL, USER, PASS);
      //STEP 4: Execute a query
      stmt = conn.createStatement();
      String sql = "Replace INTO WfJobaudit1  ( "+Column+")" +"VALUES ("+Val+")";
      //String sql = "INSERT INTO WfJobaudit1  ( "+Column+")" +"VALUES ("+Val+")";
      stmt.executeUpdate(sql);

      String Update= "";
      //coordinator_id,Workflow_id,Action_JobName ,Child_jobName,Start_time,HiveCounter_InRecord ,HiveCounter_OutRecord , Map_inRecord ,Map_outRecord ,Reducer_inrecord ,Reducer_outRecord
   }
   catch(SQLException se){
      //Handle errors for JDBC
       System.out.println(se.getMessage());
      se.printStackTrace();
   }catch(Exception e){
      //Handle errors for Class.forName
      e.printStackTrace();
   }finally{
      //finally block used to close resources
      try{
         if(stmt!=null)
            conn.close();
      }catch(SQLException se){
         //System.out.println(se.getMessage());
      }// do nothing
      try{
         if(conn!=null)
            conn.close();
      }catch(SQLException se){
          //System.out.println(se.getMessage());
         se.printStackTrace();
      }//end finally try
   }//end try
   //System.out.println("Goodbye!");
}//end main



    public static void truncateTable() {
        Connection conn = null;
        Statement stmt = null;

        try{

            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //STEP 4: Execute a query
            stmt = conn.createStatement();

            stmt.executeUpdate("truncate table WfJobaudit1 ");


            String Update= "";
            //coordinator_id,Workflow_id,Action_JobName ,Child_jobName,Start_time,HiveCounter_InRecord ,HiveCounter_OutRecord , Map_inRecord ,Map_outRecord ,Reducer_inrecord ,Reducer_outRecord

        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    conn.close();
            }catch(SQLException se){
            }// do nothing
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try
        //System.out.println("Goodbye!");
    }

}//end SqlInsert